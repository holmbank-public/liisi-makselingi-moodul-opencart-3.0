<?php
class ControllerExtensionPaymentLiisi extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/payment/liisi');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('payment_liisi', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');

		$data['entry_field_status'] = $this->language->get('entry_field_status');
		$data['entry_field_server'] = $this->language->get('entry_field_server');
		$data['entry_field_sort_order'] = $this->language->get('entry_field_sort_order');
		$data['entry_field_snd'] = $this->language->get('entry_field_snd');
		$data['entry_field_public'] = $this->language->get('entry_field_public');
		$data['entry_field_private'] = $this->language->get('entry_field_private');
		$data['entry_field_private_password'] = $this->language->get('entry_field_private_password');
		$data['entry_field_payment_confirm'] = $this->language->get('entry_field_payment_confirm');

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (isset($this->error['bank' . $language['language_id']])) {
				$data['error_bank' . $language['language_id']] = $this->error['bank' . $language['language_id']];
			} else {
				$data['error_bank' . $language['language_id']] = '';
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payment'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/liisi', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/payment/liisi', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], true);

		$this->load->model('localisation/language');

		foreach ($languages as $language) {
			if (isset($this->request->post['payment_liisi_bank' . $language['language_id']])) {
				$data['payment_liisi_bank' . $language['language_id']] = $this->request->post['payment_liisi_bank' . $language['language_id']];
			} else {
				$data['payment_liisi_bank' . $language['language_id']] = $this->config->get('payment_liisi_bank' . $language['language_id']);
			}
		}

		$data['languages'] = $languages;

		$data['payment_liisi_server'] = (isset($this->request->post['payment_liisi_server']) ? $this->request->post['payment_liisi_server'] : $this->config->get('payment_liisi_server'));

		$data['payment_liisi_public'] = (isset($this->request->post['payment_liisi_public']) ? $this->request->post['liisi_public'] : $this->config->get('payment_liisi_public'));

		$data['payment_liisi_private'] = (isset($this->request->post['payment_liisi_private']) ? $this->request->post['payment_liisi_private'] : $this->config->get('payment_liisi_private'));

		$data['payment_liisi_private_password'] = (isset($this->request->post['payment_liisi_private_password']) ? $this->request->post['payment_liisi_private_password'] : $this->config->get('payment_liisi_private_password'));

		$data['payment_liisi_snd_id'] = (isset($this->request->post['payment_liisi_snd_id']) ? $this->request->post['liisi_snd_id'] : $this->config->get('payment_liisi_snd_id'));

		$data['payment_liisi_payment_confirm'] = (isset($this->request->post['payment_liisi_payment_confirm']) ? $this->request->post['payment_liisi_payment_confirm'] : $this->config->get('payment_liisi_payment_confirm'));

		$data['payment_liisi_status'] = (isset($this->request->post['payment_liisi_status']) ? $this->request->post['payment_liisi_status'] : $this->config->get('payment_liisi_status'));

		$data['payment_liisi_sort_order'] = (isset($this->request->post['payment_liisi_sort_order']) ? $this->request->post['payment_liisi_sort_order'] : $this->config->get('payment_liisi_sort_order'));

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/liisi', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/liisi')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}


		return !$this->error;
	}
}